
import java.io.File;
import java.io.IOException;

import Fichero.Fichero;

/**
 *
 * @author CRISTIAN
 */
public class MainPlanetas {
	// TODO Esto no puede estar en el paquete por defecto, yo lo pondr�a en el
	// controller
	// TODO Nombres de los paquetes en min�scula
	// TODO Poner Especie y Galaxia en un paquete llamado modelo, renombrar
	// Funciones a controlador y fichero a ficheroHelper, ficheroManager, lo que
	// quieras.

	public static void main(String[] args) {
		File documento = new File("documentoPlanetas.txt");
		try {

			Fichero.creacion_PlanetasTXT();
			Fichero.leerFicheroTXT(documento);

		} catch (IOException ex) {
			// PERSISTENCIA
			System.out.println(ex.getMessage());

		}
	}

}// HECTOR.FLORIDO@STUCOM.COM;

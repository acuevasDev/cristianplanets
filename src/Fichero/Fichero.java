package Fichero;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import Especie.Especie;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Excepcion.ExcepcionPlanetas;
import Funciones.Manager;

/**
 *
 * @author CRISTIAN
 */
public abstract class Fichero {
	// TODO He hecho esta clase abstracta ya que no la instancias, s�lo llamas a sus
	// m�todos.
//TODO todas estas variables privadas, ya que luego tienes el devolverFile que te las devuelve
	static File VulcanoTXT;
	static File KronosTXT;
	static File NibiruTXT;
	static File AndoriaTXT;
	static String ruta = "CarpetaPlanetasTXT";

	public static void creacion_PlanetasTXT() throws IOException {

		File carpeta_Planetas = new File(ruta);
		if (!carpeta_Planetas.exists()) {
			carpeta_Planetas.mkdir();
		}
		VulcanoTXT = new File(ruta + File.separator + "Vulcano.txt");
		KronosTXT = new File(ruta + File.separator + "Kronos.txt");
		NibiruTXT = new File(ruta + File.separator + "Nibiru.txt");
		AndoriaTXT = new File(ruta + File.separator + "Andoria.txt");
		try {
			VulcanoTXT.createNewFile();

			KronosTXT.createNewFile();

			NibiruTXT.createNewFile();

			AndoriaTXT.createNewFile();
		} catch (IOException ex) {
			System.out.println("error al crear los Planetas");
		}
	}

	public static void leerFicheroTXT(File archivo) {

		FileReader fr = null;
		BufferedReader br = null;
		try {
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);
			String linea = "";
			Manager.crearPlanetas(); // TODO esto aqu� no, yo lo pondr�a en el manager que es el encargado de
										// controlar el "flow" del programa (MVC!)
			while ((linea = br.readLine()) != null) {
				String frase = linea.substring(0);
				try {

					Manager.RealizarAccionconFrase(frase); // TODO same que antes, leer deber�a encagarse de leer y
															// luego ya en el manager haces lo que tengas que hacer
				} catch (ExcepcionPlanetas ex) {
					System.out.println(ex.getMessage());
				}
			}
		} catch (IOException e) {
			System.out.println("No existe el fichero.");
		} catch (ExcepcionPlanetas ex) {
			System.out.println("Fichero mal creado");
		} finally {
			try {
				if (fr != null) {
					fr.close();
				}
			} catch (IOException e2) {
				System.out.println("No hay nada que cerrar.");
			}
		}
	}

	public static void escribirFichero(String nombreFile, List<Especie> listaEspecies) {

		File archivo = devolverFile(nombreFile);
		FileWriter lectura = null;
		PrintWriter documento = null;
		try {
			lectura = new FileWriter(archivo); // TODO cambiar de nombre estas variables, si no est�s inspirado sugiero
												// fw y pw o fileWritter printWritter
			documento = new PrintWriter(lectura);
			for (int i = 0; i < listaEspecies.size(); i++) {
				String especie = listaEspecies.get(i).toString();
				documento.println(especie); // recogemos cada paragrafo del texto(ALumno) y lo escribimos en el File
			}

		} catch (IOException e) {
			System.out.println("No se pudo escribir en el archivo"); // TODO No hagas souts aqu�, hazte una clase que se
																		// encargue de ello (se aplica a todos los sout
																		// que tienes en esta clase)
		} finally {
			try {
				if (null != lectura) {
					lectura.close();
					documento.close();
				}
			} catch (IOException e2) {
				System.out.println("No se pudo abrir el archivo");
			}
		}
	}

	public static File devolverFile(String nombreFile) {

		File documento = new File("");
		switch (nombreFile) {

		case "Kronos":
			documento = KronosTXT;
			break;
		case "Nibiru":
			documento = NibiruTXT;
			break;
		case "Andoria":
			documento = AndoriaTXT;
			break;
		case "Vulcano":
			documento = VulcanoTXT;
			break;
		}
		return documento;
	}

}

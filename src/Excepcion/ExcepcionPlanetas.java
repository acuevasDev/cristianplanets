package Excepcion;

/**
 *
 * @author CRISTIAN
 */
@SuppressWarnings("serial")
//TODO Para no memorizar los c�digos de error podr�as hacerte unas constantes globales est�ticas y estas s�, p�blicas, para poder llamarlas a ellas Ej abajo:
public class ExcepcionPlanetas extends Exception {
	private int codigoError;
	public static final int ARGUMENTOS_INVALIDO = 1;
	public static final int ESPECIE_INCORRECTA = 2;
	// TODO As� en la excepci�n t� haces throw new
	// ExcepcionPlanetas(ExcepcionPlanetas.ARGUMENTOS_INVALIDO) y no hace falta que
	// memorices nada.

	public ExcepcionPlanetas(int codigoError) {
		super();
		this.codigoError = codigoError;
	}

	public String getMessage() {
		String mensaje = "";
		switch (codigoError) {
		case 1:
			mensaje = "N.º de argumentos inválido"; // TODO CUIDADO CON LOS CAR�CTERES ESPECIALES, yo no lo veo
														// correctamente.
			break;
		case 2:
			mensaje = "Especie incorrecta";
			break;
		case 3:
			mensaje = "Planeta incorrecto";
			break;
		case 4:
			mensaje = "Dato incorrecto";
			break;
		case 5:
			mensaje = "No se puede registrar ese ser en ese planeta";
			break;
		case 6:
			mensaje = "Ya existe un ser censado con ese nombre";
			break;
		case 7:
			mensaje = "No existe ningún ser con ese nombre";
			break;
		case 8:
			mensaje = "El ser no permite ser modificado";
			break;
		case 9:
			mensaje = "Operación incorrecta";
			break;
		case 10:
			mensaje = "Edad incorrecta";
			break;
		case 11:
			mensaje = "Nivel de meditación incorrecto";
			break;
		case 12:
			mensaje = "Valor de fuerza incorrecto";
			break;
		case 13:
			mensaje = "Planeta completo (30 censados máximo)";
			break;

		}
		return mensaje;
	}

}
